'use strict';

/*
|--------------------------------------------------------------------------
| BookingSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory');

class BookingSeeder {
    async run() {
        await Factory.model('App/Models/Booking').create(10);
    }
}

module.exports = BookingSeeder;
