'use strict';

const Factory = use('Factory');

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
// const Factory = use('Factory')

// Factory.blueprint('App/Models/User', (faker) => {
//   return {
//     username: faker.username()
//   }
// })

Factory.blueprint('App/Models/Booking', (faker) => {
    return {
        property_id: 1,
        icon: 'icon',
        distance: 444,
        image: 'fkdfjdf',
        position: JSON.stringify([12121, 3232323]),
        name: faker.username(),
    };
});
