'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class BookingsSchema extends Schema {
    up() {
        this.create('bookings', (table) => {
            table.increments();
            table.string('property_id').notNullable().unique();
            table.string('name').notNullable();
            table.string('image').notNullable();
            table.string('icon').notNullable();
            table.string('distance').notNullable();
            table.json('position').notNullable();
            table.timestamps();
        });
    }

    down() {
        this.drop('bookings');
    }
}

module.exports = BookingsSchema;
