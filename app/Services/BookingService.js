/*
|--------------------------------------------------------------------------
| Packages Namespaces
|--------------------------------------------------------------------------
*/
const Booking = use('App/Models/Booking');

class BookingService {
    getAll() {
        return Booking.all();
    }

    async create(data) {
        await Booking.create(data);
    }

    async findWhere(field, value) {
        return await Booking.findByOrFail(field, value);
    }
}

module.exports = BookingService;
