/*
|--------------------------------------------------------------------------
| Packages Namespaces
|--------------------------------------------------------------------------
*/
const Env = use('Env');
const axios = require('axios');

class HereService {
    constructor() {
        this.API = Env.get('HERE_API');
        this.KEY = Env.get('HERE_API_KEY');
    }

    async makeRequest(query) {
        return await axios.get(this.API + query);
    }

    async findPlayByLocation(loglat) {
        const query = `/places/v1/discover/explore?at=${loglat}&cat=hotel&apiKey=${this.KEY}`;
        return await this.makeRequest(query)
            .then((response) => {
                return response.data;
            })
            .catch((error) => {
                console.log(error);
            });
    }
}

module.exports = HereService;
