'use strict';

/*
|--------------------------------------------------------------------------
| Services Namespaces
|--------------------------------------------------------------------------
*/
const HereService = use('App/Services/HereService');
const BookingService = use('App/Services/BookingService');

/*
|--------------------------------------------------------------------------
| Services initialization
|--------------------------------------------------------------------------
*/
const bookingService = new BookingService();

class BookingController {
    async index({ request, response }) {
        return response.send({
            data: await bookingService.getAll(),
        });
    }

    async create({ request, response }) {
        await bookingService
            .create(
                request.only([
                    'property_id',
                    'icon',
                    'distance',
                    'image',
                    'position',
                    'name',
                ])
            )
            .then((data) => {
                return response.status(200).send({
                    message: 'booking sucessful',
                    status: 200,
                });
            })
            .catch((error) => {
                return response.status(200).send({
                    message: 'booking already exist',
                    status: 400,
                });
            });
    }

    async getBooking({ request, response, params }) {
        const { id } = params;

        await bookingService
            .findWhere('property_id', id)
            .then((data) => {
                return response.status(200).send({
                    data: data,
                    message: 'booking',
                    status: 200,
                });
            })
            .catch((error) => {
                return response.status(200).send({
                    data: [],
                    message: 'booking dont exist',
                    status: 400,
                });
            });
    }
}

module.exports = BookingController;
