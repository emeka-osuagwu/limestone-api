'use strict';

/*
|--------------------------------------------------------------------------
| Services Namespaces
|--------------------------------------------------------------------------
*/
const HereService = use('App/Services/HereService');

/*
|--------------------------------------------------------------------------
| Services initialization
|--------------------------------------------------------------------------
*/
const hereService = new HereService();

class PropertyController {
    async index({ request, response }) {
        const query = request.get('at');
        console.log(query);
        if (Object.keys(query).length === 0) {
            return response.send({
                message: 'invalid query ',
            });
        }

        return response.send({
            data: await hereService.findPlayByLocation(
                query.at.split(',').toString()
            ),
        });
    }
}

module.exports = PropertyController;
