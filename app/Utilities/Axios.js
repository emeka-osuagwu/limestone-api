const Env = use('Env');
const axios = require('axios');

const HERE_API = Env.get('HERE_API');

export default axios.create({
    baseURL: HERE_API,
});
