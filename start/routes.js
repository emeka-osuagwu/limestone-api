'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

Route.get('/', () => {
    return { greeting: 'Welcome to LimeStone API V1' };
});

// Grouped
Route.group(() => {
    Route.get('/', () => {
        return { greeting: 'Welcome to LimeStone API V1' };
    });
    Route.get('property', 'PropertyController.index');
    Route.get('property/:id/bookings', 'BookingController.getBooking');
    Route.get('booking', 'BookingController.index');
    Route.post('booking', 'BookingController.create');
}).prefix('api/v1');
